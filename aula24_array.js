var inventory = [
  {name: 'apples', quantity: 2},
  {name: 'bananas', quantity: 4},
  {name: 'cherries', quantity: 5}
];

function findCherries( fruit ) {
  return fruit.name === 'cherries';
}

console.log(inventory.find(findCherries));

let array = [1,2,3];
console.log(array.copyWithin(1,0,2));

let array2 = [1,2,3];

let it = array2.entries();

for (let element of it) {
  console.log(element);
}

