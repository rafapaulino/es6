var obj1 = {
  a: 1
}

var obj2 = {
  b: 2
}

var obj3 = Object.assign(obj1, obj2);

console.log(obj3);

class Obj4 {
  constructor() {
    this.a = 1;
  }
}

class Obj5 {
  constructor() {
    this.b = 2;
  }
}

var obj4 = new Obj4();
var obj5 = new Obj5();


var obj6 = Object.assign(obj4, obj5);
console.log(obj6);

//so pega a instancia do primeiroobjeto passado

console.log(obj6 instanceof Obj4);
console.log(obj6 instanceof Obj5);

console.log(obj6.__proto__ === Obj4.prototype);
console.log(obj6.__proto__ === Obj5.prototype);


