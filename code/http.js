export class Http {
	static fetchData(url) {
		return new Promise((resolve, reject) => {
			const HTTP = new XmlHttpRequest();
			HTTP.open('GET',url);
			HTTP.onreadystatechange = function() {
				if (HTTP.readyState == XmlHttpRequest.DONE && HTTP.status == 200) {
					const RESPONSE_DATA = JSON.parse(HTTP.responseText);
					resolve(RESPONSE_DATA);
				} else if (HTTP.readyState == XmlHttpRequest.DONE) {
					reject('Something with wrong!');
				}
			};
			
			HTTP.send();
		});
	}
}