//passando varios argumentos para uma funcao rest operator
function sumUp(...toAdd)
{
  console.log(toAdd);
  let result = 0;
  for (let i = 0; i < toAdd.length; i++) {
    result += toAdd[i];
  }
  return result;
}

console.log(sumUp(10,20,30));


//spread operator
let numbers = [1,2,3,4,5];

console.log(...numbers);

console.log(Math.max(...numbers));

let obj = {
  name: 'Max',
  age: 27,
  greet: function() {
    console.log('Os ninjas tb amam!');
  }
};

let {name, age, greet: hello} = obj;

hello();

