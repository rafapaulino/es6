//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy
let person = {
  age: 27
};

let handler = {
  get: function(target, name) {
    return name in target ? target[name] : 'Non existant';
  }
};

var proxy = new Proxy(person, handler);

console.log(proxy.age);
console.log(proxy.name);
console.log(proxy.car);