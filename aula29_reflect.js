//https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Reflect
class Person {
  constructor(name) {
    this.name = name;
  }
}

function TopObj() {
  this.age = 27;
}

let person = Reflect.construct(Person, ['Max'], TopObj);

console.log(person);
console.log(person instanceof Person);
console.log(person.__proto__ == Person.prototype);
console.log(person.__proto__ == TopObj.prototype);