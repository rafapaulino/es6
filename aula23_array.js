let $array = Array.of(5,6,7,8,9);

console.log($array);

let $array2 = [5,6,7,8,9];
let newArray = Array.from($array2, val => val * 2);

console.log(newArray);

let $array3 = [5,6,7,8,9];

$array3.fill(100,1,2);

console.log($array3);


let $array4 = [5,6,7,8,20];

console.log($array4.find(val => val >= 12));