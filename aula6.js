function isEqualTo(number, compare) {
  return number === compare;
}

console.log(isEqualTo(10,10));

let name = 'Julia';
let age = 28;
let nameField = 'Nome';

let obj = {
  name: 'Max',
  age: 20,
  test() {
    console.log(this.name +', '+this.age);
  },
  [nameField]: 'Maria'
};

console.log(obj);