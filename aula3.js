//nova sintaxe de funcoes no es6
var fn = () => console.log('Ola!');
fn();

var fn2 = () => 'Oi';
console.log(fn2());

var fn3 = () => {
  let a = 2;
  let b = 3;
  return a + b;
};
console.log(fn3());

var fn4 = (a,b) => {
 return a + b;
};
console.log(fn4(10,11));

var fn5 = (a, b) => a + b;
console.log(fn5(22,44));

var fn6 = a => a + 6;
console.log(fn6(10));




