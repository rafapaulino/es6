let promise1 = new Promise(function(resolve , reject){
    setTimeout(function(){
      resolve('Resolved!');
    }, 1000);
});

let promise2 = new Promise(function(resolve , reject){
    setTimeout(function(){
      reject('Reject!');
    }, 2000);
});

Promise.all([promise1,promise2])
  .then(function(success){
    console.log('Promisse all = ' + success);
  })
  .catch(function(error){
    console.log('Promisse all = ' + error);
  });

Promise.race([promise1,promise2])
  .then(function(success){
    console.log('Promisse race = ' + success);
  })
  .catch(function(error){
    console.log('Promisse race = ' + error);
  });