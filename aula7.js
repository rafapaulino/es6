//passando varios argumentos para uma funcao rest operator
function sumUp(...toAdd)
{
  console.log(toAdd);
  let result = 0;
  for (let i = 0; i < toAdd.length; i++) {
    result += toAdd[i];
  }
  return result;
}

console.log(sumUp(10,20,30));


//spread operator
let numbers = [1,2,3,4,5];

console.log(...numbers);

console.log(Math.max(...numbers));

let numbers = [1,2,3];
let [a, b] = numbers;

console.log(a);
console.log(b);

let numbers2 = [4,5,6];
let [c, ...d] = numbers2;

console.log(d);

let x = 10;
let y = 20;

[y, x] = [x, y];
console.log(x);
console.log(y);

