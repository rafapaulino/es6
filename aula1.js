//https://jsbin.com
//var tem o escopo global
if (true) {
  var teste = 'Teste';
}
console.log(teste);

//let tem o escopo em bloco
if (true) {
  let teste2 = 'Teste 2';
}
console.log(teste2);

function doSmth() {
	age = 17;
}

let age;
doSmth();
console.log(age);


