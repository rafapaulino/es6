function log(message) {
  console.log('Log message created, message: ' + message);
}

let handler = {
  apply: function(target, thisArg, argumentsList) {
    if (argumentsList.length == 1) {
      return Reflect.apply(target, thisArg, argumentsList);
    }
  }
};

let proxy = new Proxy(log, handler);

proxy('Hello');
proxy('Hello',10);