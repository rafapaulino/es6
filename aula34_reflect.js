class Person {
  constructor(name, age) {
    this._name = name;
    this.age = age;
  }
  
  get name() {
    return this._name;
  }
}
​
let mum = {
  _name: 'Mum'
};
​
let person = new Person('Max', 27);
​
Reflect.set(person, 'name', 'Anna');
​
console.log(Reflect.get(person,'name', mum));

