class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
  
  greet(preffix) {
    console.log(preffix+' Hello my name is '+this.name);
  }
}


let person = Reflect.construct(Person, ['Max',27]);
Reflect.apply(person.greet, person, []);

Reflect.apply(person.greet, {name: 'Anna'}, ['...']);