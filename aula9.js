class Person {
  
  constructor(name)
  {
    this.name = name;
    this.job = "Web Developer";
  }
  greet() {
    console.log("Hello "+this.name+" you is a "+this.job);
  }
}

let person = new Person("Rafael");

class Rafa extends Person {
  
}

console.log(person);
person.greet();

let rafa = new Rafa("Kakaroto");
rafa.greet();

class Cachorro {
	constructor(name)
	{
		this.name = name;
	}
	latir() {
		console.log("Meu nome é: "+this.name+" e minha idade é: "+this.age);
	}
}

class Gato extends Cachorro {
	
	constructor(age) {
		super("Snoopy");
		this.age = age;
	}

	miar()
	{
		this.latir(); //super.latir() tb funciona
		this.latir();
		//se na classe que estend tiver uma funcao com o mesmo nome ai ela chama a classe que estende
		//ai para chamar a outra e so com o super
	}
}

let gato = new Gato(23);
gato.miar();


class Helper {
  static logTwice(message)
  {
    console.log(message);
    console.log(message);
  }
}

Helper.logTwice("Olá Mundo");