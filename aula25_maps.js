let cardAce = {
  name: 'Ace of Spades'
};

let cardKing = {
  name: 'The King of Fighters'
};

let deck = new Map();
deck.set('as', cardAce);
deck.set('kc', cardKing);

console.log(deck);
console.log(deck.size);
console.log(deck.get('as'));

for (key of deck.keys()) {
  console.log(key);
}

for (value of deck.values()) {
  console.log(value);
}

for (entry of deck.entries()) {
  console.log(entry);
}

deck.delete('as');

console.log(deck.get('as'));

deck.clear();
console.log(deck.size);
console.log(deck);

