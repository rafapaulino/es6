let person = {
  age: 27,
  name: 'Max'
};

let handler = {
  
};

let protoHandler = {
  get: function(target, name) {
    return name in target ? target[name] : 'Non existant';
  }
};

var proxy = new Proxy({}, handler);

var protoProxy = new Proxy(proxy, protoHandler);

Reflect.setPrototypeOf(person, protoProxy);

console.log(person.hobbies);

